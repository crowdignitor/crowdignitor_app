//
//  pitchViewController.swift
//  crowdIgnitor
//
//  Created by Vishwa Raj on 29/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class pitchViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var innerView: UIView!
    var i = 0;
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var coFounder: UILabel!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var shortPitch: UITextView!
    var gestureRecognizer = UITapGestureRecognizer()
    
    var companyData = ["CrowdIgnitor", "NextPixar"]
    var coFounderData = ["Ayush Jaiswal", "Anand Prakash"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        shortPitch!.addGestureRecognizer(gestureRecognizer)
        
        i = 0;
        // Do any additional setup after loading the view.
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        
        self.view.addGestureRecognizer(swipeUp)
        
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        
        self.view.addGestureRecognizer(swipeDown)
        
    }

    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                break
                
            case UISwipeGestureRecognizerDirection.left:
                
                break
                
            case UISwipeGestureRecognizerDirection.up:
                if (i < 1)
                {
                    i += 1
                    viewDidAppear(true)
                }
                else
                {
                    i = 0
                    viewDidAppear(true)
                }
                
                break
                
            case UISwipeGestureRecognizerDirection.down:
                if (i == 1)
                {
                    i -= 1
                    viewDidAppear(true)
                }
                else
                {
                    i = 1
                    viewDidAppear(true)
                }
                
                
            default:
                break
            }
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(companyData[i])
        coFounder.text = coFounderData[i]
        company.text = companyData[i]
        
    }
    
    @IBAction func nextCard(_ sender: UIButton) {
        
        viewDidAppear(true)
    }
   

    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        print("Tapped")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
