//
//  ePitchViewtwo.swift
//  crowdIgnitor
//
//  Created by Vishwa Raj on 24/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class ePitchViewtwo: UIViewController, UIViewControllerTransitioningDelegate {

    var i = 0 ;
    
   
    @IBOutlet weak var heading: UILabel!
    
    @IBOutlet weak var socialLikes: UIButton!
    
    @IBOutlet weak var elevatorPitch: UILabel!
    
    @IBOutlet weak var pitchImage: UIImageView!
    
    @IBOutlet weak var companyName: UILabel!
    
    @IBOutlet weak var next2Button: UIButton!
    
    let transition = CircularTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        next2Button.layer.cornerRadius = next2Button.frame.size.width / 2
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination as! ePitchView
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
    }
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = next2Button.center
        return transition
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
